#!/bin/sh
# cm: top level tool for managing CreateVM virtual machines

# Directory where the configuration files are stored.
CONFDIR=/etc/createvm

# read the CreateVM configuration file.
. $CONFDIR/createvm.conf

# define helper functions

#try_catch($cmd, $start, $error, $return) - generic shell exception handler
#$cmd    The command to run.
#$start  The string to use before running the command
#$error  The string to use if the command fails
#$return The return code to use if failed.
#Returns On failure returns $return, otherwise returns 0.
try_catch() {
return=$4
error=$3
start=$2
cmd=$1

tmp_cmd=/tmp/cmcmd.tmp

printf "%-72s" "$start: "
$cmd > $tmp_cmd 2>&1

if [ $? -ne 0 ]; then
    printf "%8s\n" "[FAILED]"
    echo >&2 "ERROR: $error"
    cat $tmp_cmd
    rm $tmp_cmd
    return $return
else
    printf "%8s\n" "[DONE]"
fi

rm $tmp_cmd
}

#getVMIP($hostname) - Returns the IP of a VM
#$hostname Hostname to lookup
#Returns the ip address of $hostname.
getVMIP() {
hostname=$1
WRONGNAME=$(echo $hostname | sed -n "s/\(.*\)-\(.*\)-\(.*\)/\2-\1-\3/p")
FORWARDING_HOST=$(find $ARNO_DIR -type f -name "$WRONGNAME-*.*"| sed -n "s|.*/$WRONGNAME-||p")
echo $FORWARDING_HOST
}

# display a per-vm note.
readNOTES() {
targets=$(find $MGMT_DIR/running_VMs/ -type f -printf "%f\n")

if [ ! -z "$1" ]; then
    {
# if a target was supplied, filter for it.
	for each in $targets
	do {
		if [ "$each" = "$1" ]; then
		    newtarget=$each;
		fi
	    }
	done;
	targets=$newtarget
    }
fi

for each in $targets
do {
	if [ -d "$VM_DIR/$each/" ]; then
	    {
		if [ -f "$VM_DIR/$each/note.txt" ]; then
		    {
 			echo -n "$each: ";
			cat "$VM_DIR/$each/note.txt";
		    }
		fi
	    }
	fi
} done;

}

# set the per-vm note.
writeNOTE() {
targets=$(find $MGMT_DIR/running_VMs/ -type f -printf "%f\n")
# since a target was supplied, filter for it to make sure its a valid target.
for each in $targets
do {
	if [ "$each" = "$1" ]; then
	    newtarget=$each;
	fi
    }
done;
targets=$newtarget

for each in $targets
do {
	if [ -d "$VM_DIR/$each/" ]; then
	    {
		if [ -f "$VM_DIR/$each/note.txt" ]; then
		    {
			if [ -w "$VM_DIR/$each/note.txt" ]; then
			    echo "$2" > $VM_DIR/$each/note.txt
			fi
		    }
		else
		    {
			echo "$2" > $VM_DIR/$each/note.txt
		    }
		fi
	    }
	fi
    } done;
}

# list pools with bridges.
lsBRIDGES() {
targets=$(find $MGMT_DIR/bridges/ -type f -printf "%f\n"| sed "s/\(.*\)-.*-.*/\1/")

if [ ! -z "$1" ]; then
    {
# if a target was supplied, filter for it.
	for each in $targets
	do {
		if [ "$each" = "$1" ]; then
		    newtarget=$each;
		fi
	    }
	done;
	targets=$newtarget
    }
fi

for each in $targets
do {
	echo $targets:
	POOL_BRIDGES=$(find $MGMT_DIR/bridges/ -type f -printf "%f\n" -name "$each*")
	for bridge in $POOL_BRIDGES
	do {
		echo " bridge" $(echo $bridge | sed "s/.*-\(.*\)-.*/\1/") is device $(echo $bridge | sed "s/.*-.*-\(.*\)/br\1/")
	    }
	done;
    }
done;

}

# Services are in the format service-domain-vm
# list VMs with services assigned
lsSERVICES() {

#If $1 is set, only list one 
if [ -z "$1" ]; then
    found=0
    for service in `ls $SERVICE_DIR/`; do

	if [ ! -z $service ]; then
	    service=`echo $service | sed 's_.*//__'`
	    service_name=`echo $service | sed 's/-.*$//'`
	    domain=`echo $service | sed 's/[a-z0-9]*-//' | sed 's/-.*//'`
	    vm=`echo $service | sed 's/[a-z]*-[a-z0-9.]*-//'`
	    ip=`getVMIP $vm`

	    if [ $found -eq 0 ]; then
		printf "%-35s%s\n" "Service/Domain" "Virtual Machine"
	    fi
	    found=1

	    printf "%-35s%s\n" "$service_name/$domain" "$vm"
	fi

    done
    if [ $found -eq 0 ]; then
	echo "No services found. Use \`cm setservice {vm} {domain} {service}' to assign a new domain/vm pair." 
    fi
fi
}

# Forwards are in the format service-domain-ip
# list IPs with services forwarded
lsFORWARDS() {

# If $1 is set, only list one
if [ -z "$1" ]; then
    found=0
    for forward in `ls $FORWARD_DIR/`; do

	if [ ! -z $forward ]; then
	    forward=`echo $forward | sed 's_.*//__'`
	    forward_name=`echo $forward | sed 's/-.*$//'`
	    domain=`echo $forward | sed 's/[a-z0-9]*-//' | sed 's/-.*//'`
	    ip=`echo $forward | sed 's/[a-z]*-[a-z0-9.]*-//'`

	    if [ $found -eq 0 ]; then
		printf "%-35s%s\n" "Forward/Domain" "IP"
	    fi
	    found=1

	    printf "%-35s%s\n" "$forward_name/$domain" "$ip"
	fi


    done
    if [ $found -eq 0 ]; then
	echo "No forwards found. Use \`cm setforward {vm} {domain} {forward}' to assign a new domain/IP pair."
    fi
fi
}

# Make a vhost proxy -> the vm.
mkforward_HTTP() {

ip=$1
domain=$2
forward=$3

vhost_file=$APACHE_DIR/sites-available/$domain.conf

if [ -f $vhost_file ]; then 
    echo "INFO: $vhost_file exists, updating."
fi

sed -e "s/CM_IP/$ip/g" -e "s/CM_DOMAIN/$domain/g" $TEMPLATE_DIR/$forward > $vhost_file

try_catch "a2ensite $domain" "Enabling vhost for $domain to $ip" "Failed to enable vhost." "4"
reload_apache2

}

rmforward_HTTP() {

ip=$1
domain=$2
forward=$3

vhost_file=$APACHE_DIR/sites-available/$domain.conf

try_catch "a2dissite $domain" "Disabling vhost for $domain to $ip" "Failed to disable vhost." "4"
try_catch "rm $vhost_file" "Removing vhost entry $vhost_file" "Failed to remove file." "4"
reload_apache2
}

rmFORWARD() {

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo >&2 "Error: Not enough arguments."
    echo "usage: rmforward vm domain service"
    exit 1
fi

ip=$1

echo "Removing $3 forward for $2 to $1..."

file=$FORWARD_DIR/$3-$2-$1

if [ ! -e "$file" ]; then
    echo >&2 "Error: Forward does not exist."
    exit 1
fi

case $3 in
    http)
	rmforward_HTTP $1 $2 $3
	;;
#   https)
#	rmservice_HTTPS $1 $2 $3
#	;;
    smtp)
#	rmservice_SMTP $1 $2 $3
	;;
#    imap)
#	mkservice_IMAP
#	;;
#   imaps)
#	mkservice_IMAPS
    *)
	echo "Error: $3 is not a supported forward method."
	exit 1
	;;
esac

rm $file

}

# Makes a new service/reassigns an existing one
mkFORWARD() {

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo >&2 "Error: Not enough arguments."
    echo "usage: setforward ip domain service"
    exit 1
fi

ip=$1
# FIXME: validate IP.

echo "Setting $3 forward for $2 to $1..."
file=$FORWARD_DIR/$3-$2-$1

if [ -f "$file" ]; then
    echo >&2 "Error: Forward already exists."
    exit 1
fi

# Confirm domain/service pair does not already exist.
ls $FORWARD_DIR/$3-$2* > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo >&2 "Error: Domain/Service pair already exists."
    exit 1
fi

case $3 in
    http)
	mkforward_HTTP $1 $2 $3
	;;
#    https)
#	mkservice_HTTPS $1 $2 $3
#	;;
    smtp)
#	mkservice_SMTP $1 $2 $3
	;;
#    imap)
#	mkservice_IMAP
#	;;
#   imaps)
#	mkservice_IMAPS
    *)
	echo "Error: $3 is not a supported forward protocol."
	exit 1
	;;
esac

touch $file

}

# restart http daemon.
reload_apache2() {
try_catch "/etc/init.d/apache2 reload" "Reloading apache2" "Failed to reload apache2." "4"
}

mkservice_SMTP() {
ip=`getVMIP $1`
vm=$1
domain=$2
service=$3

echo "Nothing to do..."

}

# Make a vhost proxy -> the vm.
mkservice_HTTP() {

ip=`getVMIP $1`
vm=$1
domain=$2
service=$3

vhost_file=$APACHE_DIR/sites-available/$domain.conf

if [ -f $vhost_file ]; then 
    echo "INFO: $vhost_file exists, updating."
fi

sed -e "s/CM_IP/$ip/g" -e "s/CM_DOMAIN/$domain/g" $TEMPLATE_DIR/$service\
   > $vhost_file

try_catch "a2ensite $domain" "Enabling vhost for $domain to $vm" "Failed to enable vhost." "4"
reload_apache2

}

rmservice_HTTP() {

ip=`getVMIP $1`
vm=$1
domain=$2
service=$3

vhost_file=$APACHE_DIR/sites-available/$domain.conf

try_catch "a2dissite $domain" "Disabling vhost for $domain to $vm" "Failed to disable vhost." "4"
try_catch "rm $vhost_file" "Removing vhost entry $vhost_file" "Failed to remove file." "4"
reload_apache2
}

rmSERVICE() {

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo >&2 "Error: Not enough arguments." 
    echo "usage: rmservice vm domain service"
    exit 1
fi 

vm=$1
if [ ! -f $MGMT_DIR/running_VMs/$vm ]; then
    echo >&2 "Warning: VM $vm does not exist."
fi

echo "Removing $3 service for $2 to $1..."

file=$SERVICE_DIR/$3-$2-$1

if [ ! -f $file ]; then
    echo >&2 "Error: Service does not exist."
    exit 1
fi

case $3 in
    http)
	rmservice_HTTP $1 $2 $3
	;;
#   https)
#	rmservice_HTTPS $1 $2 $3
#	;;
    smtp)
#	rmservice_SMTP $1 $2 $3
	;;
#    imap)
#	mkservice_IMAP
#	;;
#   imaps)
#	mkservice_IMAPS
    *)
	echo "Error: $3 is not a supported service."
	exit 1
	;;
esac

rm $file

}

# Makes a new service/reassigns an existing one
mkSERVICE() {

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo >&2 "Error: Not enough arguments." 
    echo "usage: setservice vm domain service"  
    exit 1
fi 

vm=$1
if [ ! -f $MGMT_DIR/running_VMs/$vm ]; then
    echo >&2 "Error: VM $vm does not exist."
    exit 1
fi

echo "Setting $3 service for $2 to $1..."
file=$SERVICE_DIR/$3-$2-$1

if [ -f $file ]; then
    echo >&2 "Error: Service already exists."
    exit 1
fi

# Confirm domain/service pair does not already exist.
ls $SERVICE_DIR/$3-$2* > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo >&2 "Error: Domain/Service pair already exists."
    exit 1
fi

case $3 in
    http)
	mkservice_HTTP $1 $2 $3
	;;
#    https)
#	mkservice_HTTPS $1 $2 $3
#	;;
    smtp)
#	mkservice_SMTP $1 $2 $3
	;;
#    imap)
#	mkservice_IMAP
#	;;
#   imaps)
#	mkservice_IMAPS
    *)
	echo "Error: $3 is not a supported service."
	exit 1
	;;
esac

touch $file

}

# list VMs with port forwards.
lsPORTS() {

targets=$(find $MGMT_DIR/running_VMs/ -type f -printf "%f\n")

if [ ! -z "$1" ]; then 

    {
# if a target was supplied, filter for it.
	for each in $targets
	do {
		if [ "$each" = "$1" ]; then
		    newtarget=$each;
		fi
	    }
	done;
	targets=$newtarget
    }
fi

for each in $targets
do {
	# reverse the first two items to create the name used in the ipport directory.
	WRONGNAME=$(echo $each | sed -n "s/\(.*\)-\(.*\)-\(.*\)/\2-\1-\3/p")
	FORWARDING_HOST=$(find $ARNO_DIR -type f -name "$WRONGNAME-*.*"| sed -n "s|.*/$WRONGNAME-||p")
	echo -n "$each: address is"
	if [ -z "$FORWARDING_HOST" ] ; then
	    echo " not yet assigned."
	else
	    echo " $FORWARDING_HOST."
	fi

	HOST_FORWARDS=$(find $IPPORT_DIR -type f -name "$WRONGNAME-*-*-*" | sed -n "s|.*/$WRONGNAME-||p")
        for forward in $HOST_FORWARDS
        do {
                FORWARDUID=$(echo $forward | sed -n "s/\(.*\)-.*-.*/\1/p")
                INSIDEPORT=$(echo $forward | sed -n "s/.*-\(.*\)-.*/\1/p")
                OUTSIDEPORT=$(echo $forward | sed -n "s/.*-.*-\(.*\)/\1/p")
		echo " Forward $FORWARDUID is from $OUTSIDEPORT to $INSIDEPORT"
            } done;
    } done;
}

# list VMs and their run states
lsVM () {

targets=$(find $MGMT_DIR/running_VMs/ -type f -printf "%f\n")

if [ ! -z "$1" ]; then
    {
# if a target was supplied, filter for it.
	for each in $targets
	do {
		if [ "$each" = "$1" ]; then
		    newtarget=$each;
		fi
	    }
	done;
	targets=$newtarget
    }
fi

for each in $targets
do {
	echo -n "$each: Runable,"
# if we're root, check for running screen sessions
	if [ "$(whoami)" = "root" ] ; then
	    {
		screens=$(screen -ls | grep "$each	")
		if [ ! -z "$screens" ] ; then
		    {
			echo -n " Running"
		    }
		fi
	    }
	else
	    {
# otherwise, use ps to find the kvm.
		kvms=$(ps ax | grep $KVM_PROCESS | grep "$each ")
		if [ ! -z "$kvms" ] ; then
		    {
			echo -n " Running"
		    }
		fi
	    }
	fi
	echo;
    }
done;
}

killVM() {
if [ ! -f $MGMT_DIR/running_VMs/$1 ]; then
    echo >&2 "ERROR: Running flag not found for VM $1."
    exit 1
fi

target=$1

screens=$(screen -ls | grep "$target	")
if [ ! -z "$screens" ] ;   then
    {
	echo "INFO: A screen appears to already be running for $target."
	screen -S $target -X kill
	sleep 10
	screens=$(screen -ls | grep "$target	")
	if [ ! -z "$screens" ] ; then
	    {
		echo "WARNING: screen didn't terminate when we asked it to."
		process=$(ps ax |grep SCREEN | grep -E "$target\$")
		kill -15 $process
		sleep 10
	    }
	fi
    }
fi

processes=$(ps ax | grep SCREEN | grep -E "$target\$");
if [ -z "$processes" ] ; then
    {
	VMHOSTNAME=$(echo $target | sed "s/\(.*\)-.*-.*/\1/")
	VMPOOL=$(echo $target | sed "s/.*-\(.*\)-.*/\1/")
	VMUID=$(echo $target | sed "s/.*-.*-\(.*\)/\1/")
# include the de-allocation code from the pool
	. $POOL_DIR/${VMPOOL}
	pool_vm_deallocate $VMHOSTNAME $VMPOOL $VMUID
	rm "$ARNO_DIR/$VMPOOL-$VMHOSTNAME-$VM_UID-*"
    }
else
    {
	echo "ERROR: a SCREEN process is still found for $target."
	exit 1
    }
fi
}

#Starts a VM
startVM() {
if [ ! -f $MGMT_DIR/running_VMs/$1 ]; then
    echo >&2 "ERROR: $1 does not exist."
    echo "List of available VMs:"
    lsVM
    exit 1
fi

target=$1

# Make sure user is root:
if [ `id -u` -ne 0 ]; then
    echo >&2 "Error: Only root can start VMs."
    exit 1
fi

(
    cd $VM_DIR/$target
    ./startup-kvm.sh
)

# Check to see if the VM is /really/ running.
screen -ls | grep "$target	" > /dev/null 2>&1

# Print success or failure
if [ $? -eq 0 ]; then
    echo "$target is now running."
else
    echo "$target did not start, could not find screen session."
fi

}


usage() {
echo >&2 \
"Usage: $0 <start|kill [vm]> ||\n"\
"          <list|services|forwards|ports|getip|notes [vm]> ||\n"\
"          <setservice|rmservice <vm> <domain> <service>> ||\n"\
"          <setforward|rmforward <IP> <domain> <service>> ||\n"\
"          <setnote <vm> <new note>> ||\n"\
"          <reconfig>"
    return
}

# Begin execution here

# Process arguments passed to us
case $1 in
    "list")
	lsVM $2
	;;
    "kill")
	if [ -z "$2" ] ; then
	    echo "ERROR: kill: Must provide target"
	    usage
	    exit 1
	else
	    killVM $2
	fi
	;;
    "ports")
	lsPORTS $2
	;;
    "getip")
	if [ -z "$2" ] ; then
	    targets=$(find $MGMT_DIR/running_VMs/ -type f -printf "%f\n")
	    for target in $targets; do
		printf "%-35s%s\n" $target `getVMIP $target`
	    done
	else
	    printf "%-35s%s\n" $2 `getVMIP $2`
	fi
	;;
    "bridges")
     	lsBRIDGES $2
	;;
    "notes")
	readNOTES $2
	;;
    "setnote")
	writeNOTE $2 "$3"
	;;
    "services")
	lsSERVICES $2
	;;
    "setservice")
	mkSERVICE $2 $3 $4
	;;
    "rmservice")
	rmSERVICE $2 $3 $4
	;;
    "forwards")
	lsFORWARDS $2
	;;
    "setforward")
	mkFORWARD $2 $3 $4
	;;
    "rmforward")
	rmFORWARD $2 $3 $4
	;;
    "reconfig")
	. /usr/share/createvm/helpers/service_dhcp.sh
	dhcp_regen_conf_fragment
	. /usr/share/createvm/helpers/service_dns.sh
	dns_regen_conf_fragment
	;;
    "start")
	startVM $2
	;;
    *)
	usage
esac
