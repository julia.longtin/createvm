#!/bin/sh
oldest=$(ls fai_dvd-20* | head -n 1)
newest=$(ls fai_dvd-20* | tail -n 1)
rm -f fai_dvd.iso
rm -f fai_dvd-latest.iso
ln -s $oldest fai_dvd.iso
ln -s $newest fai_dvd-latest.iso
