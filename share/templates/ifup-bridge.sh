#!/bin/sh

IP="/sbin/ip"
IFCONFIG="/sbin/ifconfig"

$IFCONFIG $1 0.0.0.0 promisc up
. ./ifup-bridge-variables.sh

BRIDGEDEV=`$BRCTL show|grep -E ^"$BRIDGE"`

if [ -n "$BRIDGEDEV" ] ; then
    {
        $BRCTL addif $BRIDGE $1
    }
else
    {
        $BRCTL addbr $BRIDGE
	$IFCONFIG $BRIDGE $BRIDGEIP netmask 255.255.255.0 up
        $BRCTL stp $BRIDGE off
        $BRCTL addif $BRIDGE $1
        $IP link set $BRIDGE up
	if [ $USEDHCP = 0 ]; then
	    /etc/init.d/isc-dhcp-server restart
	fi
	if [ $USEDNS = 0 ]; then
	    /etc/init.d/bind9 restart
	fi
    }
fi

# this is to enable VMs to use ip masquerading on the host to contact the internet, as well as to have port forwards.
/etc/init.d/arno-iptables-firewall restart
