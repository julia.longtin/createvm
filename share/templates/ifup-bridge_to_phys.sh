#!/bin/sh

IP="/sbin/ip"
IFCONFIG="/sbin/ifconfig"

$IFCONFIG $1 0.0.0.0 promisc up
. ./ifup-bridge_to_phys-variables.sh

BRIDGEDEV=`$BRCTL show|grep -E ^"$BRIDGE"`

if [ -n "$BRIDGEDEV" ] ; then
    {
        $BRCTL addif $BRIDGE $1
    }
else
    {
        $BRCTL addbr $BRIDGE
        $BRCTL stp $BRIDGE off
        $BRCTL addif $BRIDGE $1
        $BRCTL addif $BRIDGE $BRIDGEEXTDEV
        $IP link set $BRIDGE up
        $IP link set $BRIDGEEXTDEV up
    }
fi

# this is to enable the firewall to pass IP traffic over the bridge.
/etc/init.d/arno-iptables-firewall restart
