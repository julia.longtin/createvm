#!/bin/bash
#This script will start up a KVM virtual session
##################################################
#Make sure being ran by root
if [ "$UID" != "0" ]; then
    echo "Must be root"
    exit
fi

#Set HOST
if [ -n "$1" ]; then
    HOST="$1"
else
    HOST=`basename \`pwd\``
    echo "Starting host: $HOST";
    sleep 1
fi

#Test to see if there is already a screen session running for this vm
TEST=`screen -ls | grep "$HOST	"`
if [ -n "$TEST" ]; then
    echo "There is already a screen for $HOST"
    exit
fi

SCREENNAME=$HOST

# this resequencing is for the lvm code.

HOSTNAME=$(echo $HOST | cut -d'-' -f1)
if [ $HOSTNAME == "preseed" ] ; then
    POOL=$(echo $HOST|cut -d'-' -f2)
    HOSTNAME=$(echo $HOST | cut -d'-' -f3)
    VMUID=$(echo $HOST | cut -d'-' -f4)
    HOST=$HOSTNAME-$POOL-$VMUID
else
    POOL=$(echo $HOST|cut -d'-' -f2)
fi

####################################
#   Config Options
####################################

source startup-variables.sh

#Drives
BOOT=d
if [ -z $BOOTHD ]; then
	BOOTHD="/dev/$POOL/$HOST"
fi
#Type of display to use, none, curses, vnc
DISPLAY="curses"

#Other Options
OPTS="-rtc base=localtime -monitor pty -name $SCREENNAME"
PIDFILE="/var/run/kvm-$SCREENNAME.pid"

# check for required files
if [ ! -z "$NETUPSH" ]; then
    if [ ! -f "$NETUPSH" ]; then
	echo "Networking script was not found!";
	exit
    fi
fi
if [ ! -z "$NETDOWNSH" ]; then
    if [ ! -f "$NETDOWNSH" ]; then
	echo "Networking script was not found!";
	exit
    fi
fi

if [ ! -z "$DUALNIC" ]; then
    if [ ! -z "$SECONDNETUPSH" ]; then
        if [ ! -f "$SECONDNETUPSH" ]; then
	    echo "Second networking script was not found!";
	    exit
        fi
    fi
    if [ ! -z "$SECONDNETDOWNSH" ]; then
	if [ ! -f "$SECONDNETDOWNSH" ]; then
	    echo "Second networking script was not found!";
	    exit
	fi
    fi
fi

##################### Main Code Section ##############################

#Setup the physical console.
if [ "$DISPLAY" == "VNC" ] || [ "$DISPLAY" == "vnc" ]; then
    if [ ! -f /etc/qemu/.qemu-vnc ]; then
	echo "9" > /etc/qemu/.qemu-vnc
    fi
    let VNCNUM=`cat /etc/qemu/.qemu-vnc`+1
    echo "$VNCNUM" > /etc/qemu/.qemu-vnc
    QEMUDIS="-vnc :$VNCNUM -usb -usbdevice tablet"
elif [ "$DISPLAY" == "Curses" ] || [ "$DISPLAY" == "curses" ]; then
    QEMUDIS="-display curses -vga vmware"
else 
    QEMUDIS=""
fi

#Setup Network
if [ -z "$NETUPSH" ]; then
    NETWORK="-net none"
else
    if [ -z "$NETDOWNSH" ]; then
	NETWORK="-net nic,macaddr=$MAC,model=$NICMODEL -net tap,script=$NETUPSH"
    else
	NETWORK="-net nic,macaddr=$MAC,model=$NICMODEL -net tap,script=$NETUPSH,downscript=$NETDOWNSH"
    fi
fi

if [ ! -z "$DUALNIC" ]; then
    if [ -z "$SECONDNETDOWNSH" ]; then
	NETWORK="$NETWORK -net nic,macaddr=$SECONDMAC,model=$SECONDNICMODEL -net tap,script=$SECONDNETUPSH"
    else
	NETWORK="$NETWORK -net nic,macaddr=$SECONDMAC,model=$SECONDNICMODEL -net tap,script=$SECONDNETUPSH,downscript=$SECONDNETDOWNSH"
    fi
fi    

#Setup Drives
DEFAULTDRIVES="-drive file=$BOOTHD,index=0,media=disk,format=raw -drive file=$CD,index=1,media=cdrom"
if [ -n "$OPTHD" ]; then
    #Add the optional drives
    let ID=2;
    for HD in $OPTHD; do
	TMP="$OPTDRIVES -drive file=$HD,index=$ID,media=disk,format=raw "
	OPTDRIVES="$TMP"
	let ID=$ID+1
    done
fi
DRIVES="$DEFAULTDRIVES $OPTDRIVES"

### detect CPUs.

CPUS=`cat /proc/cpuinfo | grep processor | wc -l`

if [ "$CPUS" != "1" ]; then
    OPTS="$OPTS -smp threads=$CPUS"
fi

######## Command That Will be Executed
KVM="$KVM_BIN -m $MEM -boot $BOOT $DRIVES $NETWORK $OPTS $QEMUDIS"

#Create the run script
echo "Running: $KVM"
echo "$KVM" > .kvm-run
chmod +x .kvm-run

#Launch detached screen session
screen -d -m -S $SCREENNAME -t Main ./.kvm-run
#Add management console to screen session
sleep 3
MGTCONSOLE=`ls -U1 /dev/pts/ | head -1`
echo "Monitor is attached to $MGTCONSOLE"
echo "$MGTCONSOLE" > .kvm-monitor
screen -X -S $SCREENNAME screen -t Monitor /dev/pts/$MGTCONSOLE
