#!/bin/sh

#pool management file, for managing bridge devices.
# expects BRCTL, other variables.

# sane default?
MAX_BRIDGES=20

# this is empty, because for our purposes, the system always has bridges available.
bridge_available() {
    if [ ! -d /sys/module/bridge ]; then
	{
	    echo "1";
	    return 1;
	}
    fi;
    if [ ! -f "$BRCTL" ]; then
	{
	    echo "1";
	    return 1;
	}
    fi;
    echo "0";
    return 0;
}

# called by a pool script, to allocate a bridge.
# called with uid of pool, and a uid of the bridge within that pool
bridge_allocate() {
    
# check to see if we've claimed any bridges.
    if [ ! -d $MGMT_DIR/bridges ]; then
	{
# if not, create a directory to claim then in.
	    mkdir $MGMT_DIR/bridges
	}
    fi;
    success=1;
    for loop in $( seq -s " " 0 $MAX_BRIDGES); do
	{
# make sure the device has not been configured halfway, and is not in use.
	    if [ "$(bridge_check_available br$loop)" = "0" ]; then
		{
# make sure theres no claim to it...
		    if [ ! -f $MGMT_DIR/bridges/*-$loop ]; then
			{
# use it!
			    touch $MGMT_DIR/bridges/$1-$2-$loop
			    success=0;
			    break;
			}
		    fi
		}
	    fi
	}
    done;
    return $success;
}

# only called once all VMs using the pool are shut down.
bridge_deallocate() {
    POOL_BRIDGES=$(find $MGMT_DIR/bridges/ -name "$1-$2-*" 2>/dev/null)
    if [ ! -z "$POOL_BRIDGES" ]; then
	{
	    rm -f $MGMT_DIR/bridges/$1-$2-*
	}
    fi
}

# make sure the bridge device has yet to be configured, EG, is in a pristine state. otherwise fail.
bridge_check_available() {
    BRIDGE_ALREADY_SETUP=$($BRCTL show | grep -E ^"$1")
    if [ -z "$BRIDGE_ALREADY_SETUP" ]; then
	{
	    echo "0";
	    return 0;
	}
    fi
    echo "1";
    return 1;
}

#given the unique key used to allocate a bridge, print out the bridge device
# passed the pool, and the UID.
bridge_find() {
    if [ -d $MGMT_DIR/bridges/ ]; then
	TARGETS=$(find $MGMT_DIR/bridges/ -name $1-$2-* | wc -l);
	if [ $TARGETS = 1 ]; then
	    {
		echo "br"$(find $MGMT_DIR/bridges/ -name $1-$2-*| sed "s|$MGMT_DIR/bridges/$1-$2-||" 2>/dev/null)
		return 0;
	    }
	fi
    fi
    return 0;
}
