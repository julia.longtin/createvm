#!/bin/sh

# pool_lvm.sh
# Disk allocation for VMs using LVM and sparse files
# Copyright 2010 Zac Slade <krakrjak@gmail.com>
# Released under the GNU/GPL v2 and v3 or higher

# Utility function for rounding up to the nearest 512 bytes
# $1 == number to round up the the nearest 512 bytes
round_up_to_512 () {
	orig=$1
	rounded=$(($orig+512-($orig%512)))
	echo $rounded
	return 0
}

# Check if there are enough resources to allocate disks for this Vm
# $1 == pool name == volume group name
# $2 == pool disk size in bytes
# $3 == instance disk size in bytes
lvm_available() {
	# Does a VG already exist for this pool?
	if ! lvm_check_vg "$1" "$2" ; then
		# Is there enough free disk space to create the VG?
		if ! lvm_check_diskfree "$2" ; then
			echo >&2 "ERROR: Not enough space on the filesystem to create the VG $1 of size $2"
			return 1
		fi
		# No VG, but we do have enough free space to create one
		return 0
	fi

	# Is there enough free space in the VG for the LV?
	if ! lvm_check_vg_free "$1" "$3" ; then
		echo >&2 "ERROR: There is not enough space in VG $1 to create LV of size $3"
		return 1
	fi

	# In this case we do have a VG of the correct size and there is
	# enough space in the VG to fulfill the disk request
	return 0
}

# Check if there is enough space in the VG for the VM
# $1 == pool name == volume group name
# $2 == instance disk size in bytes
lvm_check_vg_free() {
	# Check the size of the volume group
	size_bytes=$(/sbin/vgs -o vg_free --noheadings --units b "$1")
	if [ $? -ne 0 ]; then
		# We don't have a volume group for this pool
		echo >&2 "ERROR: Checked for free space in non-existant VG $1"
		return 1
	fi
	size_bytes=${size_bytes%B}
	if [ $size_bytes -lt $2 ]; then
		# FIXME: Can't handle this case yet ERROR!!!
		# Need to eventually try to grow the VG if possible
		echo >&2 "ERROR: Not enough free space in VG $1 to fulfill the request of $2 in bytes"
		return 1
	elif [ $size_bytes -gt $2 ]; then
		# The VG is larger than the requested size...
		# This is probably OK.
		return 0
	fi
	return 0
}

# Check to see if there is enough free space on the file system
# to create the VG for this pool
# $1 == pool disk size in bytes
lvm_check_diskfree() {
	# WARNING: Since we are using sparse files there is a chance
	# to overcommit the filesystem as VMs begin using their disks
	# Generally this shouldn't be a problem, but it can come up
	freespace=$(/bin/df -P --block-size=1 ./|tail -n 1|awk '{print $4}')
	if [ $freespace -gt "$1" ] ; then
		# We have plenty of space for this allocation
		return 0
	fi
	# Not enough free space on the device
	echo >&2 "ERROR: Not enought free space on the filesystem to create a PV of size $1"
	return 1
}

# Perform the disk allocation using LVM and sparse files
# Create PV, VG and LV as needed to fulfill the allocation
# $1 == pool name == volume group name
# $2 == pool disk size in bytes
# $3 == hostname
# $4 == instance disk size in bytes
# $5 == installation iso sha1 hash
# $6 == VM_UID
lvm_allocate_disk() {
	local vg_name=$1
	local vg_size=$2
	local lv_name="$3-$1-$6"
	local lv_size=$4
	local iso_sha1=$5
	local host=$3
	local preseed_lv="$host-$vg_name-$iso_sha1-$lv_size"

	if lvm_check_vg $vg_name $vg_size ; then

		if lvm_check_lv $vg_name $lv_name $lv_size ; then
			# Should not already exist!!!
			echo >&2 "ERROR: LV $lv_name already exists in VG $vg_name"
			return 1
		fi

		# Check to see if we need to clone
		if lvm_check_lv $vg_name $preseed_lv $lv_size ; then
			# We need to clone from this existing LV into our usual naming scheme
			if ! lvm_copy_lv $vg_name $preseed_lv $lv_name ; then
				echo >&2 "ERROR: Cannot create snapshot LV $lv_name from VG $vg_name using source LV $preseed_lv"
				return 1
			fi
			return 0
		fi

		if ! lvm_create_lv $vg_name $preseed_lv $lv_size ; then
			echo >&2 "ERROR: Cannot create LV $preseed_lv in VG $vg_name of size $lv_size"
			return 1
		fi

		return 0
	fi

	# No existing VG.  Need to create it and the LV
	if lvm_create_vg $vg_name $vg_size ; then
		if ! lvm_create_lv $vg_name $preseed_lv $lv_size ; then
			echo >&2 "ERROR: Cannot create LV $preseed_lv in VG $vg_name of size $lv_size"
			return 1
		fi
		return 0
	fi

	# No existing VG and we could not create one
	echo >&2 "ERROR: Cannot create VG $1 of size $2"
	return 2
}

# Return the size of a logical volume or error if no LV exists
# $1 == pool name == volume group name
# $2 == host-instance == logical volume name
lvm_getsize_lv() {
	vg=$1
	lv=$2
	lv_path=$vg/$lv
	size_bytes=$(/sbin/lvs -o lv_size --noheadings --units b $lv_path)
	if [ $? -ne 0 ]; then
		echo >&2 "Could not get the size of LV $lv_path"
		return 1
	fi

	size_bytes=${size_bytes%B}
	echo $size_bytes
	return 0
}

# Check if the pool name already has a VG and if it's the right size
# $1 == pool name == volume group name
# $2 == pool disk size in bytes
lvm_check_vg() {
	# Check the size of the volume group
	size_bytes=$(/sbin/vgs -o vg_size --noheadings --units b "$1")
	if [ $? -ne 0 ]; then
		# We don't have a volume group for this pool
		return 1
	fi
	size_bytes=${size_bytes%B}
	if [ $size_bytes -lt $2 ]; then
		# FIXME: Can't handle this case yet ERROR!!!
		# Need to eventually try to grow the VG if possible
		echo >&2 "ERROR: Not enough free space in VG $1 for an LV of size $2"
		return 1
	elif [ $size_bytes -gt $2 ]; then
		# The VG is larger than the requested size...
		# This is probably OK.
		return 0
	fi
	return 0
}

# Check if we have a LV in the VG corresponding to this pool.
# If we do make sure it's the right size
# $1 == pool name == volume group name
# $2 == host-instance == logical volume name
# $3 == instance disk size in bytes
lvm_check_lv() {
	local vg_name=$1
	local lv_name=$2
	local lv_size=$3

	# Not always part of a valid file system path
	lv_path=$vg_name/$lv_name

	# Does it exist?
	sucess=$(/sbin/lvdisplay $lv_path > /dev/null 2>&1)
	if [ $? -ne 0 ]; then
		# This LV doesn't exist.  BAIL!
		return 1
	fi

	# See if the LV exists and what size it is in bytes
	size_bytes=$(lvm_getsize_lv $vg_name $lv_name)
	if [ $size_bytes -lt $lv_size ]; then
		/sbin/lvresize -L "${lv_size}B" "$lv_path" >&2
		err=$?

		if [ "$err" -ne 0 ]; then
			msg_fmt='ERROR: lvresize gave non-'
			msg_fmt=$msgfmt'zero exit code (%d).\n'
			printf >&2 "$msg_fmt" "$err" >&2
			return 1
		fi

		# Didn't resize the file system or any layer on top of the LV
		# so, continue to report failure.
		return 1
	elif [ $size_bytes -gt $lv_size ]; then
		# The LV is larger than the requested size...
		# This is probably OK.
		return 0
	fi
	return 0
}

# Create the physical volume to back the disk pool
# $1 == pool name == volume group name
# $2 == pool disk size in bytes
lvm_create_pv() {
	size=${2}
	if [ ! -f ${1}pv0 ] ; then
		filename=${1}pv0
	else
		pvnum=$(ls ${1}pv*|wc -l)
		filename=${1}pv${pvnum}
	fi
	# pad disk size by 4MiB for PVDA and VGDA reservations
	/bin/dd of=$filename bs=1 count=0 seek=$((size+4194304)) >&2
	echo "$filename"
	return
}

# Create the volume group and the physical volume
# $1 == pool name == volume group name
# $2 == pool disk size in bytes
lvm_create_vg() {
	pv=$(lvm_create_pv "$1" "$2")
	if [ $? -eq 0 ] ; then
		# Now mount the PV using losetup
		loopdev=$(/sbin/losetup -f "$pv" --show)
		if [ $? -ne 0 ]; then
			echo >&2 "ERROR: Couldn't create the loopback device for the PV $pv"
			return 1
		fi

		# Create the volume group
		/sbin/vgcreate $1 $loopdev >&2
		if [ $? -ne 0 ]; then
			echo >&2 "ERROR: Could not create the VG $1 using the loop device $loopdev"
			return 1
		fi
		return 0
	fi
	echo >&2 "ERROR: Could not create the PV $pv"
	return 1
}

# Create the logical volume for the virtual machine
# $1 == pool name == volume group name
# $2 == host-instance == logical volume name
# $3 == instance disk size in bytes
lvm_create_lv() {
	local vg_name=$1
	local lv_name=$2
	local lv_size=$(round_up_to_512 $3)
	/sbin/lvcreate -L "${lv_size}B" -n $lv_name $vg_name > /dev/null 2>&1
	return
}

# Copy the logical volume for the virtual machine
# $1 == pool name == volume group name
# $2 == source logical volume name
# $3 == dst-host-instance == destination logical volume name
lvm_copy_lv() {
	vg=$1
	src_lv=$2
	dst_lv=$3
	real_size=$(round_up_to_512 $(lvm_getsize_lv $vg $src_lv))
	/sbin/lvcreate -L "${real_size}B" -n $dst_lv -s /dev/$vg/$src_lv > /dev/null 2>&1
	return

}

# Increase the size of the instance backing disk
# $1 == pool name == volume group name
# $2 == host-instance == logical volume name
# $3 == pool disk size in bytes
lvm_grow_lv() {
	local vg_name=$1
	local lv_name=$2
	local lv_size=$3
	lv_size=$(round_up_to_512 $lv_size)
	/sbin/lvextend -L $lv_size $vg_name/$lv_name >&2
	return
}

# Unmount the loopback device and delete the file
# $1 == pool name == volume group name
# $2 == device name
lvm_destroy_pv() {
	/sbin/losetup -d "$2" >&2
	/bin/rm "$1" >&2
	return
}


# Tear down a pool's disks.  Remove the VG and all underlying PVs
# $1 == pool name == volume group name
lvm_destroy_vg() {
	for lv in $(/sbin/lvs -o lv_name --noheadings "$1"); do
		lvm_destroy_lv "$1" "$lv"
	done
	for pv in $(/sbin/pvs -o name,vg_name --noheadings|grep $1|awk '{print $1}'); do
		/sbin/vgreduce "$1" "$pv" >&2
		lvm_destroy_pv "$1" "$pv"
	done
	/sbin/vgremove -f $1 >&2
	return
}

# Destroy the instance disk
# $1 == pool name == volume group name
# $2 == host-instance == logical volume name
lvm_destroy_lv() {
	/sbin/lvremove -f "$1"/"$2" >&2
	return
}
