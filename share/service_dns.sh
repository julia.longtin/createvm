dns_allocate() {
    IPBLOCK=$(ipblock_find $1 1);
    IPBLOCKREV=$(echo $IPBLOCK| sed "s/\(.*\)\.\(.*\)\.\(.*\)/\3.\2.\1/");
    if [ ! -d $MGMT_DIR/bind9/zones ] ; then
	mkdir -p $MGMT_DIR/bind9/zones
    fi
    cat > $MGMT_DIR/bind9/zones/$1.reverse.zone <<EOF
\$TTL 3600       ; 1 hour
\$ORIGIN $IPBLOCKREV.in-addr.arpa.
@       IN      SOA     ns1.$FAKE_DOMAIN_NAME.     support.$FAKE_DOMAIN_NAME. (
                                2007102500      ; Serial number
                                86400           ; Refresh       1 day
                                7200            ; Retry         2 hours
                                3600000         ; Expire        41.67 days
                                172800  )       ; Minimum TTL   2 days
                NS      ns1.$FAKE_DOMAIN_NAME.
1       IN      PTR     faiserver.$FAKE_DOMAIN_NAME.
EOF
    cat > $MGMT_DIR/bind9/zones/$1.forward.zone <<EOF
\$TTL 3600       ; 1 hour
\$ORIGIN .
$FAKE_DOMAIN_NAME          IN SOA  ns1.$FAKE_DOMAIN_NAME. support.$FAKE_DOMAIN_NAME. (
                                2007120601      ; serial
                                10800           ; refresh (3 hours)
                                900             ; retry (15 minutes)
                                60              ; expire (1 minute)
                                50              ; minimum (50 seconds)
                                )
                        NS      ns1.$FAKE_DOMAIN_NAME.
                        A       $IPBLOCK.1
\$ORIGIN $FAKE_DOMAIN_NAME.
ns1                         A       $IPBLOCK.1
faiserver                   A       $IPBLOCK.1
EOF
    if [ ! -d $MGMT_DIR/bind9/conffiles ] ; then
	mkdir -p $MGMT_DIR/bind9/conffiles
    fi
    cat > $MGMT_DIR/bind9/conffiles/$1.conf <<EOF
view "$1" {
        match-clients { $IPBLOCK/24; };
        recursion yes;
        zone "$FAKE_DOMAIN_NAME." IN {
                type master;
                file "$MGMT_DIR/bind9/zones/$1.forward.zone";
                update-policy { grant rndc-key name $FAKE_DOMAIN_NAME. A; grant rndc-key subdomain $FAKE_DOMAIN_NAME. A; }; 
        };
        #internal reverse dns
        zone "$IPBLOCKREV.in-addr.arpa." IN {
                type master;
                file "$MGMT_DIR/bind9/zones/$1.reverse.zone";
                update-policy { grant rndc-key subdomain $IPBLOCKREV.in-addr.arpa. A; };
        };
};
EOF

    dns_regen_conf_fragment
    return 0;
}

dns_deallocate() {
    if [ -f $MGMT_DIR/bind9/zones/$1.reverse.zone ]; then
	rm $MGMT_DIR/bind9/zones/$1.reverse.zone;
    fi
    if [ -f $MGMT_DIR/bind9/zones/$1.forward.zone ]; then
	rm $MGMT_DIR/bind9/zones/$1.forward.zone
    fi
    if [ -f $MGMT_DIR/bind9/conffiles/$1.conf ]; then
	rm $MGMT_DIR/bind9/conffiles/$1.conf
    fi

    # fixme: this leaves dns broken. what about the autogen file?
    return 0;
}

dns_regen_conf_fragment() {
# trunicate our target file
    cp /dev/null $BIND_DIR/named.conf.autogen

    for each in $(find $MGMT_DIR/bind9/conffiles/ -type f); do
	echo include \"$each\"";" >> $BIND_DIR/named.conf.autogen
    done;
    return 0;
}
