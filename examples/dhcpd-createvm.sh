#!/bin/sh

# a script for updating flags, and restarting ip masquerading.

# arguments:
# $1 = the action dhcpd is performing.
# $2 = the pool uid.
# $3 = the path createvm has been installed in.
# $4 = the IP of the client.
# $5 = the MAC address of the client.

PREFIX=$3

CONFDIR=/etc/createvm

# read the CreateVM configuration file.
. $CONFDIR/createvm.conf

# include the pool definition script.
. $POOL_DIR/$2

# get_hostname()
# get the hostname, from the UID and the pool
# $1 = the pool UID
# $2 = the vm UID
get_hostname() {
HOSTNAME=$(find $MGMT_DIR/running_VMs/ -name "*-$1-$2"| sed -n "s/.*\/\(.*\)-.*-.*/\1/p")
echo -n $HOSTNAME
if [ -z "$HOSTNAME" ]; then
    return 1
fi
return 0
}


POOL_UID=$2
VM_IP=$4
VM_MAC=$5

VM_UID=$(pool_uid_from_mac $VM_MAC)
VM_HOSTNAME=$(get_hostname $POOL_UID $VM_UID)

if [ ! -f "$ARNO_DIR/$POOL_UID-$VM_HOSTNAME-$VM_UID-$VM_IP" ]; then
    {
	touch "$ARNO_DIR/$POOL_UID-$VM_HOSTNAME-$VM_UID-$VM_IP"
	if [ "$(ls $ARNO_DIR/$POOL_UID-$VM_HOSTNAME-$VM_UID-* | wc -l)" == "1" ]; then
	    {
		/etc/init.d/arno-iptables-firewall restart
	    }
	fi
    }
fi

if [ "$(ls $ARNO_DIR/$POOL_UID-$VM_HOSTNAME-$VM_UID-* | wc -l)" != "1" ]; then
    {
	rm $( find $ARNO_DIR -name $POOL_UID-$VM_HOSTNAME-$VM_UID-* | sed "s=$ARNO_DIR/$POOL_UID-$VM_HOSTNAME-$VM_UID-$VM_IP\$==")
	SERVICES="$(find $SERVICE_DIR/ -name *-$VM_HOSTNAME-$POOL_UID-$VM_UID)"
	if [ "$SERVICES" != "" ]; then
	    {
		for SERVICE in $SERVICES;
		do {
			echo $SERVICE >> /tmp/jtmp

			tuple=$(echo $SERVICE | sed "s=.*/==")
			PROTO=$(echo $tuple | sed "s/\([a-zA-Z]*\)-.*/\1/")
			DOMAIN=$(echo $tuple | sed "s/[a-zA-Z]*-\(.*\)-$VM_HOSTNAME-$POOL_UID-$VM_UID/\1/")
			cm rmservice $VM_HOSTNAME-$POOL_UID-$VM_UID $DOMAIN $PROTO
			cm setservice $VM_HOSTNAME-$POOL_UID-$VM_UID $DOMAIN $PROTO
		    } done;
	    }
	fi
	/etc/init.d/arno-iptables-firewall restart
    }
fi