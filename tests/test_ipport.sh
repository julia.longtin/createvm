#!/bin/sh

MGMT_DIR=create_vm
. ./pool_ipport.sh

#test for no output, and no error flag if we're asking for a block thats not yet been allocated.
echo -n TEST1: 
TESTOUT=$(ipport_find )
if [ $? != 0 ]; then
    echo FAIL
fi
if [ ! -z $TESTOUT ]; then
    echo FAIL
fi

echo -n "port available: "
result=$(ipport_available 2200 2202)
echo "."
ipport_allocate pool hostname 1 hostname1A 22 2200 2202
ipport_find pool hostname1A
ipport_deallocate pool hostname1A
echo $?
#ipblock_allocate 2200 2202
#ipblock_find pool range2
#ipblock_deallocate pool range2

#for loop in $(seq -s " " 16 31); do
#    {
#	if [ $(ipblock_check_available 172.$loop.0) = 0 ]; then
#	    echo "0"
#	else
#	    echo "1"
#	fi
#} done;

