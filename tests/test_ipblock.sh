#!/bin/sh

MGMT_DIR=create_vm
. ./pool_ipblocks.sh

#test for no output, and no error flag if we're asking for a block thats not yet been allocated.
echo -n TEST1: 
TESTOUT=$(ipblock_find pool range1)
if [ $? != 0 ]; then
    echo FAIL
fi
if [ ! -z $TESTOUT ]; then
    echo FAIL
fi

ipblock_allocate pool range1
ipblock_find pool range1
ipblock_deallocate pool range1
echo $?
ipblock_allocate pool range2
ipblock_find pool range2
ipblock_deallocate pool range2

for loop in $(seq -s " " 16 31); do
    {
	if [ $(ipblock_check_available 172.$loop.0) = 0 ]; then
	    echo "0"
	else
	    echo "1"
	fi
} done;

