CREATEVM(1M)
============
:doctype: manpage

NAME
----
createvm - KVM virtual machine creation and provisioning system

SYNOPSIS
--------
*createvm* - <'vm_hostname'> <'pool_name'> <'install_iso'>

DESCRIPTION
-----------
The createvm(1M) command allows you to provision and instantiate new virtual machines using KVM or QEMU.  Uses NAT for networking and has a pluggable disk backend controlled via configuration files.

OPTIONS
-------
*vm_hostname*::
    Name of host type to build from the installation ISO.
*pool_name*::
    Name of the virtual machine pool to carve out the virtual machine from.
*install_iso::
    Name of the ISO file to install the virtual machine with.

EXAMPLES
--------
Create an fai server in the tearoff pool::
createvm qemufaiserver tearoff fai.iso
Create an openemr appliance in the tearoff pool::
createvm qemuoemrserver tearoff fai.iso
Create a mediawiki appliance in the tearoff pool::
createvm qemuwikiserver tearoff fai.iso

SEE ALSO
--------
cm(1)

REPORTING BUGS
--------------
Please report bugs via email.  Patches welcome.  Having a published git tree with your patches makes it easy to integrate your patches.

COPYRIGHT
---------
(c) 2011 Zac Slade <krakrjak@gmail.com>

(c) 2011 Julia Longtin <julia.longtin@gmail.com>
